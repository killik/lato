<?php

namespace App\Http\Controllers\Auth\Operator;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Auth\ConfirmPasswordController as Controller;

class ConfirmPasswordController extends Controller
{
    /**
     * Where to redirect users when the intended url fails.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::DASHBOARD;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:operator');
    }
}
