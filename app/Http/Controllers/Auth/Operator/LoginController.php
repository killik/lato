<?php

namespace App\Http\Controllers\Auth\Operator;

use App\Http\Controllers\Auth\LoginController as Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected $redirectTo = "/dashboard";

    public function __construct() {
        $this->middleware('guest:operator')->except('logout');
    }

    public function username(): string
    {
        return 'username';
    }

    public function showLoginForm()
    {
        return view('auth.operator.login');
    }

    protected function guard()
    {
        return Auth::guard('operator');
    }
}
