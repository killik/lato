<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Operator\DestroyRequest;
use App\Http\Requests\Dashboard\Operator\PatchRequest;
use App\Http\Requests\Dashboard\Operator\StoreRequest;
use App\Operator;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class OperatorController extends Controller
{
    public function index(): View
    {
        $operators = Operator::orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.operator.index', compact('operators'));
    }

    public function show(Operator $operator): View
    {
        return view('dashboard.operator.show', compact('operator'));
    }

    public function create(): View
    {
        $roles = Role::whereGuardName('operator')->get()->all();

        return view('dashboard.operator.form.create', compact('roles'));
    }

    public function store(StoreRequest $req)
    {
        Operator::create($req->all())->assignRole($req->role);

        return redirect(route('operator.index'))->with(['success' => trans('dashboard.operator.stored')]);
    }

    public function edit(Operator $operator): View
    {
        return view('dashboard.operator.form.edit', compact('operator'));
    }

    public function update(PatchRequest $req, Operator $operator): RedirectResponse
    {
        $operator->update($req->all());

        return redirect(route('operator.index'))->with(['success' => trans('dashboard.operator.updated')]);
    }

    public function destroy(DestroyRequest $req, Operator $operator): RedirectResponse
    {
        $operator = $req->route('operator');

        $operator->delete();

        return redirect(route('operator.index'))->with(['success' => trans('dashboard.operator.removed')]);
    }
}
