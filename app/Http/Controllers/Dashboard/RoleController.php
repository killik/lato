<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Role\DestroyRequest;
use App\Http\Requests\Dashboard\Role\PatchRequest;
use App\Http\Requests\Dashboard\Role\StoreRequest;
use App\Operator;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(): View
    {
        $roles = Role::orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.role.index', compact('roles'));
    }

    public function show(Role $role): View
    {
        return view('dashboard.role.show', compact('role'));
    }

    public function operator(Role $role): View
    {
        $operators = Operator::role($role->name)->paginate(4);

        return view('dashboard.role.operator', compact('role', 'operators'));
    }

    public function create(): View
    {
        return view('dashboard.role.form.create');
    }

    public function store(StoreRequest $req)
    {
        Role::create($req->all());

        return redirect(route('role.index'))->with(['success' => trans('dashboard.role.stored')]);
    }

    public function edit(Role $role): View
    {
        return view('dashboard.role.form.edit', compact('role'));
    }

    public function update(PatchRequest $req, Role $role): RedirectResponse
    {
        $role->update($req->all());

        return redirect(route('role.index'))->with(['success' => trans('dashboard.role.updated')]);
    }

    public function destroy(DestroyRequest $req, Role $role): RedirectResponse
    {
        $role = $req->route('role');

        $role->delete();

        return redirect(route('role.index'))->with(['success' => trans('dashboard.role.removed')]);
    }

    public function operatorDestroy(DestroyRequest $req, Role $role, Operator $operator): RedirectResponse
    {
        $role = $req->route('role');

        $operator->removeRole($role->name);

        return redirect(route('role.index'))->with(['success' => trans('dashboard.role.operatorRemoved')]);
    }
}
