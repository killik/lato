<?php

namespace App\Http\Requests\Dashboard\Operator;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can('generate new operator');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:4|max:24',
            'username' => 'required|string|unique:operators|min:3|max:24',
            'email' => 'required|email|unique:operators',
            'password' => 'required|string|min:6',
            'role' => 'required|exists:roles,name'
        ];
    }
}
