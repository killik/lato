<?php

namespace App\Lingu\Abstracts\Rules\Create;

use Illuminate\Contracts\Validation\Rule;

abstract class UniqueOnGuard implements Rule
{
    protected string $__guard;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $guard)
    {
        $this->__guard = $guard;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.unique');
    }
}
