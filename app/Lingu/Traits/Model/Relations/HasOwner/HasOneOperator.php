<?php

namespace App\Lingu\Traits\Model\Relations\HasOwner;

use App\Operator;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait HasOneOperator {
    public function owner(): HasOne
    {
        return $this->hasOne(Operator::class);
    }
}
