<?php

namespace App;

use App\Lingu\Abstracts\Model\Authenticatable;

class Operator extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];
}
