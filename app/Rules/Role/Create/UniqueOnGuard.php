<?php

namespace App\Rules\Role\Create;

use App\Lingu\Abstracts\Rules\Create\UniqueOnGuard as Rule;
use Spatie\Permission\Models\Role;

class UniqueOnGuard extends Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return !Role::whereName($value)->whereGuardName($this->__guard)->exists();
    }
}
