@extends('layouts.sudo')

@section('icon', 'hashtag')
@section('title')
    Role
    <a href="{{ route('role.index') }}" class="float-right text-decoration-none text-dark">
        <i class="fa fa-arrow-left"></i> Back
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('role')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <tbody>
                <tr>
                    <td class="text-left"><i class="fa fa-calendar-alt"></i> Created At</td>
                    <td>{{ $operator->created_at }}</td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-signature"></i> Name</td>
                    <td>
                        {{ $operator->name }}
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-user-shield"></i> User Name</td>
                    <td>
                        <span class="badge badge-{{ $operator->hasRole('super user') ? 'danger':'primary' }}">
                            {{ $operator->username }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-hashtag"></i> Roles</td>
                    <td class="text-wrap">
                        @forelse ($operator->roles as $role)
                            <a href="{{ route('role.show', $role->id) }}" class="badge badge-{{ $role->name == 'super user' && $role->guard_name == 'operator'?'danger':'primary' }}">
                                {{ $role->name }}
                            </a>
                        @empty
                            <span class="badge badge-danger">
                                no role
                            </span>
                        @endforelse
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-frog"></i> Permissions</td>
                    <td class="text-wrap">
                        @if ($operator->name == 'super user' && $operator->guard_name == 'operator')
                            <span class="badge badge-danger">
                                all permissions
                            </span>
                        @else
                            @forelse ($operator->permissions as $permission)
                                <a href="{{ route('permission.show', $permission->id) }}" class="badge badge-primary">
                                    {{ $permission->name }}
                                </a>
                            @empty
                                <span class="badge badge-danger">
                                    no permissions
                                </span>
                            @endforelse
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
