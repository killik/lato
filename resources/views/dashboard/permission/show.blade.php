@extends('layouts.sudo')

@section('icon', 'hashtag')
@section('title')
    Role
    <a href="{{ route('role.index') }}" class="float-right text-decoration-none text-dark">
        <i class="fa fa-arrow-left"></i> Back
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('role')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <tbody>
                <tr>
                    <td class="text-left"><i class="fa fa-calendar-alt"></i> Created At</td>
                    <td>{{ $permission->created_at }}</td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-signature"></i> Name</td>
                    <td>
                        <span class="badge badge-primary">
                            {{ $permission->name }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-user-shield"></i> Guard</td>
                    <td>
                        <span class="badge badge-secondary">
                            {{ $permission->guard_name }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-hashtag"></i> Roles</td>
                    <td class="text-wrap">
                        @forelse ($permission->roles as $role)
                            <a href="{{ route('role.show', $role->id) }}" class="badge badge-{{ $role->name == 'super user' && $role->guard_name == 'operator'?'danger':'primary' }}">
                                {{ $role->name }}
                            </a>
                        @empty
                            <span class="badge badge-danger">
                                no roles
                            </span>
                        @endforelse
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
