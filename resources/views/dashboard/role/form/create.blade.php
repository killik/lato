@extends('layouts.sudo')

@section('icon', 'hashtag')

@section('title')
Generate new Role
<a href="{{ route('role.index') }}" class="float-right text-decoration-none text-dark">
    <i class="fa fa-arrow-left"></i> Back
</a>
@endsection

@section('body')
<form method="POST" action="{{ route('role.store') }}" class="p-3">
    @if (session('success'))
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif

    @csrf

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="guard_name" class="col-md-4 col-form-label text-md-right">Guard</label>

        <div class="col-md-6">
            <input id="guard_name" type="text" class="form-control @error('guard_name') is-invalid @enderror" name="guard_name" value="{{ old('guard_name') }}" autocomplete="guard_name" autofocus>

            @error('guard_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">Send</button>
        </div>
    </div>
</form>

@endsection
