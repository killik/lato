@extends('layouts.sudo')

@section('icon', 'hashtag')
@section('title')
    Role:
    <span class="badge badge-{{ $role->name == 'super user' && $role->guard_name == 'operator'?'danger':'primary' }}">
        {{ $role->name }}
    </span>
    <a href="{{ route('role.show', $role->id) }}" class="float-right text-decoration-none text-dark">
        <i class="fa fa-arrow-left"></i> Back
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('operator')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <thead>
                <tr>
                    <td><i class="fa fa-wheelchair"></i></td>
                    <td><i class="fa fa-signature"></i> Name</td>
                    <td><i class="fa fa-envelope"></i> Email</td>
                    <td><i class="fa fa-calendar-alt"></i> Created At</td>
                    <td><i class="fa fa-fire-alt"></i> Aksi</td>
                </tr>
            </thead>
            <tbody>
                @php $no = 1; @endphp
                @forelse ($operators as $row)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>
                        <span class="badge badge-{{ $row->hasRole('super user') ? 'danger':'primary' }}">
                            {{ $row->name }}
                        </span>
                    </td>
                    <td>
                        <span class="badge badge-secondary">
                            {{ $row->email }}
                        </span>
                    </td>
                    <td>{{ $row->created_at }}</td>
                    <td>
                        <form action="{{ route('role.operatorDestroy', [$role->id, $row->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="role" value="{{ $row->name }}"/>
                            <a class="btn btn-primary btn-sm" href="{{ route('operator.show', $row->id) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    {!! $operators->links() !!}
</div>
@endsection
