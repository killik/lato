@extends('layouts.sudo')

@section('icon', 'hashtag')
@section('title')
    Role
    <a href="{{ route('role.index') }}" class="float-right text-decoration-none text-dark">
        <i class="fa fa-arrow-left"></i> Back
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('role')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <tbody>
                <tr>
                    <td class="text-left"><i class="fa fa-calendar-alt"></i> Created At</td>
                    <td>{{ $role->created_at }}</td>
                </tr>
                <tr>
                    <td class="text-left">
                        <a href="{{ route('role.operator', $role->id) }}" class="text-dark text-decoration-none">
                            <i class="fa fa-user"></i> Pengguna
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('role.operator', $role->id) }}" class="text-dark text-decoration-none">
                            {{ $role->users->count() }} Akun
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-signature"></i> Name</td>
                    <td>
                        <span class="badge badge-{{ $role->name == 'super user' && $role->guard_name == 'operator'?'danger':'primary' }}">
                            {{ $role->name }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-user-shield"></i> Guard</td>
                    <td>
                        <span class="badge badge-secondary">
                            {{ $role->guard_name }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-frog"></i> Permissions</td>
                    <td class="text-wrap">
                        @if ($role->name == 'super user' && $role->guard_name == 'operator')
                            <span class="badge badge-danger">
                                all permissions
                            </span>
                        @else
                            @forelse ($role->permissions as $permission)
                                <a href="{{ route('permission.show', $permission->id) }}" class="badge badge-primary">
                                    {{ $permission->name }}
                                </a>
                            @empty
                                <span class="badge badge-danger">
                                    no permissions
                                </span>
                            @endforelse
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
