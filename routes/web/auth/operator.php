<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Auth\\Operator'], function () {
    Route::get('login', "LoginController@showLoginForm")->name('auth.operator.login');
    Route::post('login', "LoginController@login");

    Route::post('logout', "LoginController@logout")->name('auth.operator.logout');
});

Route::group(['prefix' => 'password'], base_path('routes/web/auth/operator/password.php'));
