<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Dashboard', 'middleware' => 'auth:operator'], function () {
    Route::get('/', fn () => view('dashboard.index'))->name('Dashboard');
    Route::group(['prefix' => 'sudo', 'middleware' => 'role:super user'], base_path('routes/web/dashboard/sudo.php'));
});
