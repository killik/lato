<?php

use Illuminate\Support\Facades\Route;

Route::resource('operator', 'OperatorController');

Route::resource('permission', 'PermissionController');

Route::resource('role', 'RoleController');
Route::delete('role/{role}/operator/{operator}', 'RoleController@operatorDestroy')->name('role.operatorDestroy');
Route::get('role/{role}/operator', 'RoleController@operator')->name('role.operator');


Route::get('/', fn () => redirect(route('role.index')));
